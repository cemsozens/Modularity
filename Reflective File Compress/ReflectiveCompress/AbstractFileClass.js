var AbstractFile = /** @class */ (function () {
  function AbstractFile() {}
  AbstractFile.prototype.compress = function () {
    console.log("Abstract File compress");
  };
  return AbstractFile;
})();

var FancyCompression = function() {
    console.log("Fancy Compression");
}

var StandartCompression = function() {
    console.log("Standart Compression");
}

const handler = {
  set(obj, prop, value) {
    Reflect.set(...arguments);
    if (obj.size >= 1000 && obj.type === "video") {
        Reflect.set(obj, "compress", FancyCompression)
    }
    else {
        Reflect.set(obj, "compress", StandartCompression)
    }
  }
};

const file = new AbstractFile();
var proxy1 = new Proxy(file, handler);

proxy1.size = 1000;
proxy1.type = "video";

proxy1.compress();


proxy1.size = 50;
proxy1.compress();