import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

import java.security.InvalidParameterException;

@Aspect
public class TeamSubRuleAspect {

    int substitutedPlayerCount = 0;
    private static final int MAX_SUB_COUNT = 3;

    @Pointcut("execution(boolean substitute(*,*))")
    public void playerSubstitutePoint() {}

    @Before("playerSubstitutePoint()")
    public void checkConditions(JoinPoint jp) throws Exception {
        if (substitutedPlayerCount >= MAX_SUB_COUNT) {
            throw new Exception("MAX_SUB_COUNT already reached");
        }
    }

    @After("playerSubstitutePoint()")
    public void setSubstituted(JoinPoint jp) throws Exception {
        if (substitutedPlayerCount >= MAX_SUB_COUNT) {
            throw new Exception("MAX_SUB_COUNT already reached");
        }
        substitutedPlayerCount++;
    }
    
}
