import java.util.List;

public class Team {

    List<Player> activePlayers;
    List<Player> allPlayers;

    public void setActivePlayers(List<Player> activePlayers) {
        this.activePlayers = activePlayers;
    }

    public void setAllPlayers(List<Player> allPlayers) {
        this.allPlayers = allPlayers;
    }

    public boolean substitute( Player playerIn, Player playerOut) {

        if (!activePlayers.contains(playerOut)) {
            return false;
        }
        if (!allPlayers.contains(playerIn)) {
            return false;
        }

        activePlayers.remove(playerOut);
        activePlayers.add(playerIn);
        return true;
    }
}
