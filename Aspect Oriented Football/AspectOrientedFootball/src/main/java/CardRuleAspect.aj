import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;

import java.security.InvalidParameterException;

@Aspect
public class CardRuleAspect {

    int cardState = 0; // 0 is no card, 1 is yellow, 2 is red

    @Pointcut("execution(boolean tackle())")
    public void playerTacklePoint(){}

    @Before("playerTacklePoint()")
    public void beforeTackle() throws Exception {
        if (cardState == 2) {
            throw new Exception("Player cannot be on the pitch!");
        }
        System.out.println("Hard tackle there! Referee must punish player with red card.");
    }

    @After("playerTacklePoint()")
    public void afterTackle(JoinPoint jp) {
        if (cardState != 2) {
            cardState = 2;
            System.out.println("Player Banned");
        }
    }


    @Pointcut("execution(boolean handball())")
    public void playerHandballPoint() {}

    @Before("playerHandballPoint()")
    public void beforeHandBall() throws Exception {
        if (cardState == 2) {
            throw new Exception("Player cannot be on the pitch!");
        }
        System.out.println("Hand Ball! Referee will punish player with yellow cart.");
    }

    @After("playerHandballPoint()")
    public void afterHandball(JoinPoint jp) throws Exception {
        if (cardState == 2) {
            throw new Exception("Player cannot be on the pitch!");
        }

        cardState++;
        System.out.println("Player Got a yellow card");
        if (cardState == 2) { // Check state after
            System.out.println("Player Banned");
        }

    }
}
