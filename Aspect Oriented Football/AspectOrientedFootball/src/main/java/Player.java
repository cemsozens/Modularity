public class Player {

    Team team;

    public Player(Team team) {
        this.team = team;
    }

    public boolean tackle() {
        /*
        * Things to do according to tackle
        */
        return true;
    }

    public boolean handball() {
        /*
         * Things to do according to handball
         */
        return true;
    }

    public boolean offside() {
        /*
         * Things to do according to offside
         */
        return true;
    }
}
