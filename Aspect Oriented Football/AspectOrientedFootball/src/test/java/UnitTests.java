import org.junit.Assert;
import org.junit.Test;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UnitTests {
    @Test
    public void TackleWithRules() {
        System.out.println("-----TackleWithRules-----");
        Team team = new Team();
        Player player1 = new Player(team);
        player1.tackle();
    }

    @Test(expected = Exception.class)
    public void TackleWithRulesException() {
        System.out.println("-----TackleWithRulesException-----");
        Team team = new Team();
        Player player1 = new Player(team);
        player1.tackle();
        player1.tackle();
    }

    @Test
    public void HandBallWithRules() {
        System.out.println("-----HandBallWithRules-----");
        Team team = new Team();
        Player player = new Player(team);
        player.handball();
        player.handball();
    }

    @Test(expected = Exception.class)
    public void HandBallWithRulesException() {
        System.out.println("-----HandBallWithRulesException-----");
        Team team = new Team();
        Player player = new Player(team);
        player.tackle();

        //Should raise exception
        player.handball();

    }

    @Test(expected = Exception.class)
    public void SubstituteWithRules() {
        System.out.println("-----SubstituteWithRules-----");
        Team team = new Team();
        Player player1 = new Player(team);
        Player player2 = new Player(team);

        Player player3 = new Player(team);
        Player player4 = new Player(team);

        Player player5 = new Player(team);
        Player player6 = new Player(team);

        Player player7 = new Player(team);
        Player player8 = new Player(team);

        List<Player> activePlayer = new ArrayList<>();
        activePlayer.add(player1);
        activePlayer.add(player3);
        activePlayer.add(player5);
        activePlayer.add(player7);
        team.setActivePlayers(activePlayer);

        List<Player> allPlayers = new ArrayList<>();
        allPlayers.add(player2);
        allPlayers.add(player4);
        allPlayers.add(player6);
        allPlayers.add(player8);
        team.setAllPlayers(activePlayer);

        team.substitute(player2, player1);
        team.substitute(player4, player3);
        team.substitute(player6, player5);


        team.substitute(player8, player7);
    }

}